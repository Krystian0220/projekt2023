<?php declare(strict_types=1);

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class PageController extends AbstractController
{
    private ArticleRepository $articleRepository;
    private $authenticationUtils;

    public function __construct(ArticleRepository $articleRepository, AuthenticationUtils $authenticationUtils)
    {
        $this->articleRepository = $articleRepository;
        $this->authenticationUtils = $authenticationUtils;
    }

    #[Route('/', name: 'app_homepage')]
    public function homepage(): Response
    {

        $error = $this->authenticationUtils->getLastAuthenticationError();
        return $this->render(
            'Pages/index.html.twig',
            [
            'title' => 'Infonews',
                'error' => $error,

            ]
        );

    }
    #[Route('/menu', name: 'app_menu')]
    public function menu(): Response
    {   $lastEmail = $this->authenticationUtils->getLastUsername();
        $articles = $this->articleRepository->findAll();
        return $this->render(
            'Pages/zalogowany.html.twig',
            [
            'articles' => $articles,
                'LastEmail'=> $lastEmail,
            ]
        );
    }

    #[Route('/edit/{id}', name: 'app_edit')]
    public function edit($id): Response
    {
        $articles = $this->articleRepository->findBy(['id' => $id]);
        if (!$articles) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        return $this->render(
            'Pages/edit.html.twig',
            [
                'articles' => $articles,
            ]
        );
    }

    #[Route('/add', name: 'app_add')]
    public function addnews(): Response
    {
        return $this->render(
            'Pages/dodaj.html.twig',
            [
            'title' => 'Infonews',
            ]
        );

    }
}
