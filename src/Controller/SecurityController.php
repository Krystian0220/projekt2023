<?php
// src/Controller/SecurityController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private AuthenticationUtils $authenticationUtils;
    private Security $security;

    public function __construct(AuthenticationUtils $authenticationUtils, Security $security)
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->security = $security;
    }

    #[Route('/login', name: 'app_login')]
    public function login(): Response
    {
        // Sprawdź, czy użytkownik jest już zalogowany
        if ($this->getUser()) {
            return $this->redirectToRoute('app_menu', ['email' => $this->getUser()->getEmail()]);
        }

        // Pobierz ostatni błąd logowania, jeśli istnieje
        $error = $this->authenticationUtils->getLastAuthenticationError();

        // Pobierz nazwę użytkownika (email), jeśli została wprowadzona
        $lastEmail = $this->authenticationUtils->getLastUsername();

        return $this->render('security/zalogowany.html.twig', [
            'last_email' => $lastEmail,
            'error' => $error,
        ]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(SessionInterface $session): void
    {
        $session->invalidate();
        // Przenieś użytkownika do strony głównej
        $this->redirectToRoute('app_homepage');

    }
}
